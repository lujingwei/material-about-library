package com.danielstone.materialaboutlibrarydemo.snack;

import ohos.agp.text.Font;
import ohos.utils.Parcel;
import ohos.utils.Sequenceable;

class Snack implements Sequenceable {

    final String mMessage;

    final String mActionMessage;

    final int mActionIcon;

    //    final Parcelable mToken;
    final Sequenceable mToken;

    final short mDuration;

    //    final ColorStateList mBtnTextColor;
    final int mBtnTextColor;

    //    final ColorStateList mBackgroundColor;
    final int mBackgroundColor;

    final int mHeight;

    //    Typeface mTypeface;
    Font mTypeface;
    boolean mIsBottom;

    //    Snack(String message, String actionMessage, int actionIcon,
//                 Parcelable token, short duration, ColorStateList textColor,
//                 ColorStateList backgroundColor, int height, Typeface typeFace) {
    public Snack(String message, String actionMessage, int actionIcon,
                 Sequenceable token, short duration, int textColor, int backgroundColor, int height, Font typeFace, boolean isBottom) {
        mMessage = message;
        mActionMessage = actionMessage;
        mActionIcon = actionIcon;
        mToken = token;
        mDuration = duration;
        mBtnTextColor = textColor;
        mBackgroundColor = backgroundColor;
        mHeight = height;
        mTypeface = typeFace;
        mIsBottom = isBottom;
    }
    // reads data from parcel
    public Snack(Parcel p) {
        mMessage = p.readString();
        mActionMessage = p.readString();
        mActionIcon = p.readInt();
//        mToken = p.readParcelable(p.getClass().getClassLoader());
        mToken = p.readSequenceableList(Sequenceable.class).get(0);
        mDuration = (short) p.readInt();
//        mBtnTextColor = p.readParcelable(p.getClass().getClassLoader());
        mBtnTextColor = p.readInt();
//        mBackgroundColor = p.readParcelable(p.getClass().getClassLoader());
        mBackgroundColor = p.readInt();
        mHeight = p.readInt();
//        mTypeface = (Font) p.readValue(p.getClass().getClassLoader());
        mTypeface = (Font) p.readValue();
    }

    // writes data to parcel
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(mMessage);
        out.writeString(mActionMessage);
        out.writeInt(mActionIcon);
//        out.writeParcelable(mToken, 0);
        out.writeSequenceable(mToken);
        out.writeInt((int) mDuration);
//        out.writeParcelable(mBtnTextColor, 0);
//        out.writeParcelable(mBackgroundColor, 0);
        out.writeInt(mBtnTextColor);
        out.writeInt(mBackgroundColor);
        out.writeInt(mHeight);
        out.writeValue(mTypeface);
        out.writeBoolean(mIsBottom);
    }

    public int describeContents() {
        return 0;
    }

    // creates snack array
    public static final Sequenceable.Producer<Snack> CREATOR = new Sequenceable.Producer<Snack>() {
        public Snack createFromParcel(Parcel in) {
            return new Snack(in);
        }

        public Snack[] newArray(int size) {
            return new Snack[size];
        }
    };

    @Override
    public boolean marshalling(Parcel parcel) {
        return false;
    }

    @Override
    public boolean unmarshalling(Parcel parcel) {
        return false;
    }
//    public static final Parcelable.Creator<Snack> CREATOR = new Parcelable.Creator<Snack>() {
//        public Snack createFromParcel(Parcel in) {
//            return new Snack(in);
//        }
//
//        public Snack[] newArray(int size) {
//            return new Snack[size];
//        }
//    };
}
