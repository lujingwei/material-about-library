package com.danielstone.materialaboutlibrarydemo.items;

import com.danielstone.materialaboutlibrarydemo.*;
import com.danielstone.materialaboutlibrarydemo.holders.ItemViewHolder;
import com.danielstone.materialaboutlibrarydemo.utils.MyViewTypeManager;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Image;
import ohos.agp.components.Text;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.utils.LayoutAlignment;
import ohos.global.resource.Resource;

public class MyCustomItem extends MaterialAboutItem {

    public static final int ALIGNMENT_TOP = 0;
    public static final int ALIGNMENT_MIDDLE = 1;
    public static final int ALIGNMENT_BOTTOM = 2;
    private String text = null;
    private int textRes = 0;
    private String subText = null;
    private int subTextRes = 0;
    private int iconRes = 0;
    private boolean showIcon = true;
    private int iconAlignment;
    private OnClickListener onClickListener = null;

    public MyCustomItem(MyCustomItem item) {
        this.id = item.getId();
        this.text = item.getText();
        this.textRes = item.getTextRes();
        this.subText = item.getSubText();
        this.subTextRes = item.getSubTextRes();
        this.iconRes = item.getIconRes();
        this.showIcon = item.shouldShowIcon();
        this.iconAlignment = item.getIconAlignment();
        this.onClickListener = item.getOnClickListener();
    }

    private MyCustomItem(Builder builder) {
        this.text = builder.text;
        this.textRes = builder.textRes;

        this.subText = builder.subText;
        this.subTextRes = builder.subTextRes;

        this.iconRes = builder.iconRes;

        this.showIcon = builder.showIcon;

        this.iconAlignment = builder.iconAlignment;

        this.onClickListener = builder.onClickListener;
    }

    public MyCustomItem(String text, String subText, int iconRes, OnClickListener onClickListener) {
        this.text = text;
        this.subText = subText;
        this.iconRes = iconRes;
        this.onClickListener = onClickListener;
    }

    public MyCustomItem(String text, String subText, int iconRes) {
        this.text = text;
        this.subText = subText;
        this.iconRes = iconRes;
    }

    public MyCustomItem(int textRes, int subTextRes, int iconRes, OnClickListener onClickListener) {
        this.textRes = textRes;
        this.subTextRes = subTextRes;
        this.iconRes = iconRes;
        this.onClickListener = onClickListener;
    }

    public MyCustomItem(int textRes, int subTextRes, int iconRes) {
        this.textRes = textRes;
        this.subTextRes = subTextRes;
        this.iconRes = iconRes;
    }

    public static ItemViewHolder getViewHolder(Component view) {
        return new MyCustomItemViewHolder(view);
    }

    public static void setupItem(MyCustomItemViewHolder holder, MyCustomItem item, Component parent) {
        String text = item.getText();
        int textRes = item.getTextRes();

        holder.text.setVisibility(Component.VISIBLE);
        if (text != null) {
            holder.text.setText(text);
        } else if (textRes != 0) {
            holder.text.setText(textRes);
        } else {
            holder.text.setVisibility(Component.HIDE);
        }

        String subText = item.getSubText();
        int subTextRes = item.getSubTextRes();

        holder.subText.setVisibility(Component.VISIBLE);
        if (subText != null) {
            holder.subText.setText(subText);
        } else if (subTextRes != 0) {
            holder.subText.setText(subTextRes);
        } else {
            holder.subText.setVisibility(Component.HIDE);
        }

        if (item.showIcon) {
            holder.icon.setVisibility(Component.VISIBLE);
            int drawableRes = item.getIconRes();
            try {
                Resource resource = parent.getContext().getResourceManager().getResource(drawableRes);
                holder.icon.setImageElement(new PixelMapElement(resource));
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            holder.icon.setVisibility(Component.HIDE);
        }

        DirectionalLayout.LayoutConfig params = (DirectionalLayout.LayoutConfig) holder.icon.getLayoutConfig();
        switch (item.getIconAlignment()) {
            case MyCustomItem.ALIGNMENT_TOP:
                params.alignment = LayoutAlignment.TOP;
                break;
            case MyCustomItem.ALIGNMENT_MIDDLE:
                params.alignment = LayoutAlignment.VERTICAL_CENTER;
                break;
            case MyCustomItem.ALIGNMENT_BOTTOM:
                params.alignment = LayoutAlignment.BOTTOM;
                break;
        }
        holder.icon.setLayoutConfig(params);

//        if (item.getOnClickListener() != null) {
//            TypedValue outValue = new TypedValue();
//            context.getTheme().resolveAttribute(R.attr.selectableItemBackground, outValue, true);
//            holder.view.setBackgroundResource(outValue.resourceId);
//            holder.onClickListener = item.getOnClickListener();
//            holder.view.setSoundEffectsEnabled(true);
//        } else {
//            TypedValue outValue = new TypedValue();
//            context.getTheme().resolveAttribute(R.attr.selectableItemBackground, outValue, false);
//            holder.view.setBackgroundResource(outValue.resourceId);
//            holder.onClickListener = null;
//            holder.view.setSoundEffectsEnabled(false);
//        }
        holder.onClickListener = item.getOnClickListener();
    }

    @Override
    public int getType() {
        return MyViewTypeManager.CUSTOM_ITEM;
    }

    @Override
    public String getDetailString() {
        return "MyCustomItem{" +
                "text=" + text +
                ", textRes=" + textRes +
                ", subText=" + subText +
                ", subTextRes=" + subTextRes +
                ", iconRes=" + iconRes +
                ", showIcon=" + showIcon +
                ", iconAlignment=" + iconAlignment +
                '}';
    }

    @Override
    public MyCustomItem clone() {
        return new MyCustomItem(this);
    }

    public String getText() {
        return text;
    }

    public int getTextRes() {
        return textRes;
    }

    public String getSubText() {
        return subText;
    }

    public int getSubTextRes() {
        return subTextRes;
    }

    public int getIconRes() {
        return iconRes;
    }

    public boolean shouldShowIcon() {
        return showIcon;
    }

    public int getIconAlignment() {
        return iconAlignment;
    }

    public OnClickListener getOnClickListener() {
        return onClickListener;
    }

    public interface OnClickListener {
        void onClick();
    }

    public static class MyCustomItemViewHolder extends ItemViewHolder implements Component.ClickedListener {
        public Component view;
        public Image icon;
        public Text text;
        public Text subText;
        public OnClickListener onClickListener;

        MyCustomItemViewHolder(Component view) {
            super(view);
            this.view = view;
            icon = (Image) view.findComponentById(ResourceTable.Id_mal_item_image);
            text = (Text) view.findComponentById(ResourceTable.Id_mal_item_text);
            subText = (Text) view.findComponentById(ResourceTable.Id_mal_action_item_subtext);

            view.setClickedListener(this);
            onClickListener = null;
        }

        @Override
        public void onClick(Component v) {
            if (onClickListener != null) {
                onClickListener.onClick();
            }
        }
    }

    public static class Builder {
        OnClickListener onClickListener;
        private String text = null;
        private int textRes = 0;
        private String subText = null;
        private int subTextRes = 0;
        private int iconRes = 0;
        private boolean showIcon = true;
        private int iconAlignment = IconAlignment.ALIGNMENT_MIDDLE.getValue();

        public Builder text(String text) {
            this.text = text;
            this.textRes = 0;
            return this;
        }

        public Builder text(int text) {
            this.textRes = text;
            this.text = null;
            return this;
        }

        public Builder subText(String subText) {
            this.subText = subText;
            this.subTextRes = 0;
            return this;
        }

        public Builder subText(int subTextRes) {
            this.subText = null;
            this.subTextRes = subTextRes;
            return this;
        }

        public Builder subTextHtml(String subTextHtml) {
//            this.subText = Html.fromHtml(subTextHtml, Html.FROM_HTML_MODE_LEGACY);
            this.subText = subTextHtml;
            this.subTextRes = 0;
            return this;
        }

        public Builder icon(int iconRes) {
            this.iconRes = iconRes;
            return this;
        }

        public Builder showIcon(boolean showIcon) {
            this.showIcon = showIcon;
            return this;
        }

        public Builder setIconAlignment(int iconAlignment) {
            this.iconAlignment = iconAlignment;
            return this;
        }

        public Builder setOnClickListener(OnClickListener onClickListener) {
            this.onClickListener = onClickListener;
            return this;
        }

        public MyCustomItem build() {
            return new MyCustomItem(this);
        }
    }
}
