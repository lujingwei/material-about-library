package com.danielstone.materialaboutlibrarydemo.items;

import com.danielstone.materialaboutlibrarydemo.IconAlignment;
import com.danielstone.materialaboutlibrarydemo.MaterialAboutItemOnClickAction;
import com.danielstone.materialaboutlibrarydemo.ResourceTable;
import com.danielstone.materialaboutlibrarydemo.holders.ItemViewHolder;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Image;
import ohos.agp.components.Text;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.utils.LayoutAlignment;
import ohos.global.resource.Resource;
import ohos.multimodalinput.event.TouchEvent;

public class ActionItem extends MaterialAboutItem {

    public static final int ALIGNMENT_TOP = 0;
    public static final int ALIGNMENT_MIDDLE = 1;
    public static final int ALIGNMENT_BOTTOM = 2;

    private String text = null;
    private int textRes = 0;
    private String subText = null;
    private int subTextRes = 0;
    private int iconRes = 0;
    private boolean showIcon = true;
    private int iconAlignment;
    private MaterialAboutItemOnClickAction onClickAction = null;
    private MaterialAboutItemOnClickAction onLongClickAction = null;

    private ActionItem(Builder builder) {
        super();
        this.text = builder.text;
        this.textRes = builder.textRes;
        this.subText = builder.subText;
        this.subTextRes = builder.subTextRes;
        this.iconRes = builder.iconRes;
        this.showIcon = builder.showIcon;
        this.iconAlignment = builder.iconAlignment;
        this.onClickAction = builder.onClickAction;
        this.onLongClickAction = builder.onLongClickAction;
    }

    public ActionItem(ActionItem item) {
        this.id = item.getId();
        this.text = item.getText();
        this.textRes = item.getTextRes();
        this.subText = item.getSubText();
        this.subTextRes = item.getSubTextRes();
        this.iconRes = item.getIconRes();
        this.showIcon = item.showIcon;
        this.iconAlignment = item.iconAlignment;
        this.onClickAction = item.onClickAction;
        this.onLongClickAction = item.onLongClickAction;
    }

    public static ItemViewHolder getViewHolder(Component view) {
        return new ActionItemViewHolder(view);
    }

    public static void setupItem(ActionItemViewHolder holder, ActionItem item, Component parent) {
        String text = item.getText();
        int textRes = item.getTextRes();

        holder.text.setVisibility(Component.VISIBLE);
        if (text != null) {
            holder.text.setText(text);
        } else if (textRes != 0) {
            holder.text.setText(textRes);
        } else {
            holder.text.setVisibility(Component.HIDE);
        }

        String subText = item.getSubText();
        int subTextRes = item.getSubTextRes();

        holder.subText.setVisibility(Component.VISIBLE);
        if (subText != null) {
            holder.subText.setText(subText);
        } else if (subTextRes != 0) {
            holder.subText.setText(subTextRes);
        } else {
            holder.subText.setVisibility(Component.HIDE);
        }

        if (item.showIcon) {
            holder.icon.setVisibility(Component.VISIBLE);
            int drawableRes = item.getIconRes();
            try {
                Resource resource = parent.getContext().getResourceManager().getResource(drawableRes);
                holder.icon.setImageElement(new PixelMapElement(resource));
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            holder.icon.setVisibility(Component.HIDE);
        }

        DirectionalLayout.LayoutConfig params = (DirectionalLayout.LayoutConfig) holder.icon.getLayoutConfig();
        switch (item.iconAlignment) {
            case ActionItem.ALIGNMENT_TOP:
                params.alignment = LayoutAlignment.TOP;
                break;
            case ActionItem.ALIGNMENT_MIDDLE:
                params.alignment = LayoutAlignment.VERTICAL_CENTER;
                break;
            case ActionItem.ALIGNMENT_BOTTOM:
                params.alignment = LayoutAlignment.BOTTOM;
                break;
        }
        holder.icon.setLayoutConfig(params);

//        if (item.getOnClickAction() != null || item.getOnLongClickAction() != null) {
//            TypedValue outValue = new TypedValue();
//            context.getTheme().resolveAttribute(R.attr.selectableItemBackground, outValue, true);
//            holder.view.setBackgroundResource(outValue.resourceId);
//        } else {
//            holder.view.setBackgroundResource(0);
//        }

        holder.setOnClickAction(item.getOnClickAction());
        holder.setOnLongClickAction(item.getOnLongClickAction());
    }

    public String getText() {
        return text;
    }

    public int getTextRes() {
        return textRes;
    }

    public String getSubText() {
        return subText;
    }

    public ActionItem setSubText(String subText) {
        this.subTextRes = 0;
        this.subText = subText;
        return this;
    }

    public int getSubTextRes() {
        return subTextRes;
    }

    public int getIconRes() {
        return iconRes;
    }

    public boolean isShowIcon() {
        return showIcon;
    }

    public MaterialAboutItemOnClickAction getOnClickAction() {
        return onClickAction;
    }

    public ActionItem setOnClickAction(MaterialAboutItemOnClickAction onClickAction) {
        this.onClickAction = onClickAction;
        return this;
    }

    public MaterialAboutItemOnClickAction getOnLongClickAction() {
        return onLongClickAction;
    }

    public ActionItem setOnLongClickAction(MaterialAboutItemOnClickAction onLongClickAction) {
        this.onLongClickAction = onLongClickAction;
        return this;
    }

    @Override
    public int getType() {
        return 0;
    }

    @Override
    public String getDetailString() {
        return "ActionItem{" +
                "text='" + text + '\'' +
                ", textRes=" + textRes +
                ", subText='" + subText + '\'' +
                ", subTextRes=" + subTextRes +
                ", iconRes=" + iconRes +
                ", showIcon=" + showIcon +
                ", iconAlignment=" + iconAlignment +
                '}';
    }

    @Override
    public MaterialAboutItem clone() {
        return new ActionItem(this);
    }

    public static class ActionItemViewHolder extends ItemViewHolder implements Component.ClickedListener,
            Component.LongClickedListener {
        public final Component view;
        public final Image icon;
        public final Text text;
        public final Text subText;
        private MaterialAboutItemOnClickAction onClickAction;
        private MaterialAboutItemOnClickAction onLongClickAction;

        ActionItemViewHolder(Component view) {
            super(view);
            this.view = view;
            icon = (Image) view.findComponentById(ResourceTable.Id_mal_item_image);
            text = (Text) view.findComponentById(ResourceTable.Id_mal_item_text);
            subText = (Text) view.findComponentById(ResourceTable.Id_mal_action_item_subtext);
        }

        public void setOnClickAction(MaterialAboutItemOnClickAction onClickAction) {
            this.onClickAction = onClickAction;
            view.setClickedListener(onClickAction != null ? this : null);
        }

        public void setOnLongClickAction(MaterialAboutItemOnClickAction onLongClickAction) {
            this.onLongClickAction = onLongClickAction;
            if (onLongClickAction != null) {
                view.setTouchEventListener(new Component.TouchEventListener() {
                    private long oldMills;

                    @Override
                    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
                        switch (touchEvent.getAction()) {
                            case TouchEvent.PRIMARY_POINT_DOWN:
                                oldMills = System.currentTimeMillis();
                            case TouchEvent.PRIMARY_POINT_UP:
                                long newMills = System.currentTimeMillis();
                                if (newMills - oldMills >= 1000) {
                                    onLongClickAction.onClick();
                                }
                        }
                        return true;
                    }
                });
            }
        }

        @Override
        public void onClick(Component v) {
            if (onClickAction != null) {
                onClickAction.onClick();
            }
        }

        @Override
        public void onLongClicked(Component component) {
            if (onLongClickAction != null) {
                onLongClickAction.onClick();
            }
        }
    }

    public static class Builder {
        MaterialAboutItemOnClickAction onClickAction = null;
        MaterialAboutItemOnClickAction onLongClickAction = null;
        private String text = null;
        private int textRes = 0;
        private String subText = null;
        private int subTextRes = 0;
        private int iconRes = 0;
        private boolean showIcon = true;
        private int iconAlignment = IconAlignment.ALIGNMENT_MIDDLE.getValue();

        public Builder text(String text) {
            this.text = text;
            this.textRes = 0;
            return this;
        }

        public Builder text(int text) {
            this.textRes = text;
            this.text = null;
            return this;
        }

        public Builder subText(String subText) {
            this.subText = subText;
            this.subTextRes = 0;
            return this;
        }

        public Builder subText(int subTextRes) {
            this.subText = null;
            this.subTextRes = subTextRes;
            return this;
        }

        public Builder subTextHtml(String subTextHtml) {

//            this.subText = Html.fromHtml(subTextHtml, Html.FROM_HTML_MODE_LEGACY);
            this.subText = subTextHtml;
            this.subTextRes = 0;
            return this;
        }

        public Builder icon(int iconRes) {
            this.iconRes = iconRes;
            return this;
        }

        public Builder showIcon(boolean showIcon) {
            this.showIcon = showIcon;
            return this;
        }

        public Builder setIconAlignment(int iconAlignment) {
            this.iconAlignment = iconAlignment;
            return this;
        }

        public Builder setOnClickAction(MaterialAboutItemOnClickAction onClickAction) {
            this.onClickAction = onClickAction;
            return this;
        }

        public Builder setOnLongClickAction(MaterialAboutItemOnClickAction onLongClickAction) {
            this.onLongClickAction = onLongClickAction;
            return this;
        }

        public ActionItem build() {
            return new ActionItem(this);
        }
    }
}
