package com.danielstone.materialaboutlibrarydemo.items;

import com.danielstone.materialaboutlibrarydemo.holders.ItemViewHolder;
import com.danielstone.materialaboutlibrarydemo.MaterialAboutItemOnClickAction;
import com.danielstone.materialaboutlibrarydemo.ResourceTable;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.Text;
import ohos.agp.components.element.PixelMapElement;
import ohos.global.resource.Resource;

public class TitleItem extends MaterialAboutItem {

    private String text = null;
    private int textRes = 0;
    private String desc = null;
    private int descRes = 0;
    private int iconRes = 0;
    private MaterialAboutItemOnClickAction onClickAction = null;
    private MaterialAboutItemOnClickAction onLongClickAction = null;

    private TitleItem(TitleItem.Builder builder) {
        super();
        this.text = builder.text;
        this.textRes = builder.textRes;
        this.desc = builder.desc;
        this.descRes = builder.descRes;
        this.iconRes = builder.iconRes;
        this.onClickAction = builder.onClickAction;
        this.onLongClickAction = builder.onLongClickAction;
    }

    public TitleItem(TitleItem item) {
        this.id = item.getId();
        this.text = item.getText();
        this.textRes = item.getTextRes();
        this.desc = item.getDesc();
        this.descRes = item.getDescRes();
        this.iconRes = item.getIconRes();
        this.onClickAction = item.getOnClickAction();
        this.onLongClickAction = item.getOnLongClickAction();
    }

    public static ItemViewHolder getViewHolder(Component view) {
        return new TitleItemViewHolder(view);
    }

    public static void setupItem(TitleItemViewHolder holder, TitleItem item, Component parent) {

        String text = item.getText();
        int textRes = item.getTextRes();

        holder.text.setVisibility(Component.VISIBLE);
        if (text != null) {
            holder.text.setText(text);
        } else if (textRes != 0) {
            holder.text.setText(textRes);
        } else {
            holder.text.setVisibility(Component.HIDE);
        }

        String desc = item.getDesc();
        int descRes = item.getDescRes();

        holder.desc.setVisibility(Component.VISIBLE);
        if (desc != null) {
            holder.desc.setText(desc);
        } else if (descRes != 0) {
            holder.desc.setText(descRes);
        } else {
            holder.desc.setVisibility(Component.HIDE);
        }

        int drawableRes = item.getIconRes();
        if (drawableRes != 0) {
            try {
                Resource resource = parent.getContext().getResourceManager().getResource(drawableRes);
                holder.icon.setImageElement(new PixelMapElement(resource));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

//        if (item.getOnClickAction() != null || item.getOnLongClickAction() != null) {
//            TypedValue outValue = new TypedValue();
//            context.getTheme().resolveAttribute(R.attr.selectableItemBackground, outValue, true);
//            holder.view.setBackgroundResource(outValue.resourceId);
//        } else {
//            holder.view.setBackgroundResource(0);
//        }

        holder.setOnClickAction(item.getOnClickAction());
        holder.setOnLongClickAction(item.getOnLongClickAction());

    }

    public String getText() {
        return text;
    }

    public int getTextRes() {
        return textRes;
    }

    public String getDesc() {
        return desc;
    }

    public int getDescRes() {
        return descRes;
    }

    public int getIconRes() {
        return iconRes;
    }

    public MaterialAboutItemOnClickAction getOnClickAction() {
        return onClickAction;
    }

    public MaterialAboutItemOnClickAction getOnLongClickAction() {
        return onLongClickAction;
    }

    @Override
    public int getType() {
        return 1;
    }

    @Override
    public String getDetailString() {
        return "TitleItem{" +
                "text='" + text + '\'' +
                ", textRes=" + textRes +
                ", desc='" + desc + '\'' +
                ", descRes=" + descRes +
                ", iconRes=" + iconRes +
                '}';
    }

    @Override
    public MaterialAboutItem clone() {
        return new TitleItem(this);
    }

    public static class TitleItemViewHolder extends ItemViewHolder implements Component.ClickedListener, Component.LongClickedListener {
        public Component view;
        public Image icon;
        public Text text;
        public Text desc;
        private MaterialAboutItemOnClickAction onClickAction;
        private MaterialAboutItemOnClickAction onLongClickAction;

        TitleItemViewHolder(Component view) {
            super(view);
            this.view = view;
            icon = (Image) view.findComponentById(ResourceTable.Id_mal_item_image);
            text = (Text) view.findComponentById(ResourceTable.Id_mal_item_text);
            desc = (Text) view.findComponentById(ResourceTable.Id_mal_item_desc);
        }

        public void setOnClickAction(MaterialAboutItemOnClickAction onClickAction) {
            this.onClickAction = onClickAction;
            if (onClickAction != null) {
                view.setClickedListener(this);
            } else {
                view.setClickable(false);
            }
        }

        public void setOnLongClickAction(MaterialAboutItemOnClickAction onLongClickAction) {
            this.onLongClickAction = onLongClickAction;
            if (onLongClickAction != null) {
                view.setLongClickedListener(this);
            } else {
                view.setLongClickable(false);
            }
        }

        @Override
        public void onClick(Component v) {
            if (onClickAction != null) {
                onClickAction.onClick();
            }
        }

        @Override
        public void onLongClicked(Component v) {
            if (onLongClickAction != null) {
                onLongClickAction.onClick();
            }
        }
    }

    public static class Builder {
        MaterialAboutItemOnClickAction onClickAction = null;
        MaterialAboutItemOnClickAction onLongClickAction = null;

        private String text = null;
        private int textRes = 0;
        private String desc = null;
        private int descRes = 0;
        private int iconRes = 0;

        public TitleItem.Builder text(String text) {
            this.text = text;
            this.textRes = 0;
            return this;
        }

        public TitleItem.Builder text(int text) {
            this.textRes = text;
            this.text = null;
            return this;
        }

        public TitleItem.Builder desc(String desc) {
            this.desc = desc;
            this.descRes = 0;
            return this;
        }

        public TitleItem.Builder desc(int desc) {
            this.descRes = desc;
            this.desc = null;
            return this;
        }

        public TitleItem.Builder icon(int iconRes) {
            this.iconRes = iconRes;
            return this;
        }

        public TitleItem.Builder setOnClickAction(MaterialAboutItemOnClickAction onClickAction) {
            this.onClickAction = onClickAction;
            return this;
        }

        public TitleItem.Builder setOnLongClickAction(MaterialAboutItemOnClickAction onLongClickAction) {
            this.onLongClickAction = onLongClickAction;
            return this;
        }

        public TitleItem build() {
            return new TitleItem(this);
        }
    }
}
