package com.danielstone.materialaboutlibrarydemo;

import com.danielstone.materialaboutlibrarydemo.items.ActionItem;
import com.danielstone.materialaboutlibrarydemo.model.MaterialAboutCard;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Text;
//import ohos.agp.components.webengine.WebView;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;
import ohos.bundle.BundleInfo;
import ohos.utils.IntentConstants;
import ohos.utils.net.Uri;
import org.jetbrains.annotations.Nullable;

public class ConvenienceBuilder {

    /**
     * Creates an item with version info read from the PackageManager for current application
     * @param c Context
     * @param iconRes icon's resourceId
     * @param text text of title
     * @param includeVersionCode is include VersionCode
     * @return an item with version info
     */
    public static ActionItem createVersionActionItem(Context c, int iconRes, String text, boolean includeVersionCode) {
        String versionName = "";
        int versionCode = 0;
        try {
            BundleInfo info = c.getBundleManager().getBundleInfo(c.getBundleName(), 0);
            versionName = info.getVersionName();
            versionCode = info.getVersionCode();
        } catch (Exception ignored) {
            // This shouldn't happen.
        }
        return new ActionItem.Builder()
                .text(text)
                .subText(versionName + (includeVersionCode ? " (" + versionCode + ")" : ""))
                .icon(iconRes)
                .build();
    }

    public static MaterialAboutItemOnClickAction createWebViewDialogOnClickAction(Context c, String dialogTitle, String htmlString, boolean isStringUrl, boolean supportZoom) {

//        String str = c.getString(ResourceTable.String_mal_close);
        String str = "Close";
        return createWebViewDialogOnClickAction(c, dialogTitle, str, htmlString, isStringUrl, supportZoom);
    }

    public static MaterialAboutItemOnClickAction createWebViewDialogOnClickAction(Context c, String dialogTitle, String dialogNegativeButton, String htmlString, boolean isStringUrl, boolean supportZoom) {
        return () -> {
            Component view;
//            if (isStringUrl) {
//                WebView wv = new WebView(c);
//                wv.load(htmlString);
//                view = wv;
//            }else{
                Text text = new Text(c);
                int matchContent = ComponentContainer.LayoutConfig.MATCH_CONTENT;
                DirectionalLayout.LayoutConfig config = new DirectionalLayout.LayoutConfig(matchContent, matchContent);
                config.alignment = LayoutAlignment.CENTER;
                config.setMargins(50, 50, 50, 50);
                text.setLayoutConfig(config);
                text.setTextSize(50);
                text.setTextColor(Color.BLACK);
                text.setText("暂不支持WebView,无法打开网页");
                view = text;
//            }

            CommonDialog alert = new CommonDialog(c);
            alert.setSize(900, 400);
            alert.setTitleText(dialogTitle);
            alert.setContentCustomComponent(view);

            alert.setButton(0, dialogNegativeButton, (iDialog, i) -> alert.hide());

            alert.show();
        };
    }


    public static ActionItem createWebViewDialogItem(Context c, int iconRes, String text, @Nullable String subText, String dialogTitle, String htmlString, boolean isStringUrl, boolean supportZoom) {
//        String str = c.getString(ResourceTable.String_mal_close);
        String str = "Close";
        return createWebViewDialogItem(c, iconRes, text, subText, dialogTitle, str, htmlString, isStringUrl, supportZoom);
    }

    public static ActionItem createWebViewDialogItem(Context c, int iconRes, String text, @Nullable String subText, String dialogTitle, String dialogNegativeButton, String htmlString, boolean isStringUrl, boolean supportZoom) {
        return new ActionItem.Builder()
                .text(text)
                .subText(subText)
                .icon(iconRes)
                .setOnClickAction(createWebViewDialogOnClickAction(c, dialogTitle, dialogNegativeButton, htmlString, isStringUrl, supportZoom))
                .build();
    }


    public static MaterialAboutItemOnClickAction createWebsiteOnClickAction(final Context c, final Uri websiteUrl) {
        return () -> {
            Intent intents = new Intent();
            Operation operation = new Intent.OperationBuilder()
                    .withUri(Uri.parse(websiteUrl.toString()))
                    .withAction(IntentConstants.ACTION_SEARCH)
                    .build();
            intents.setOperation(operation);
            c.startAbility(intents, 0);
        };
    }

    public static ActionItem createWebsiteActionItem(Context c, int iconRes, String text, boolean showAddress, final Uri websiteUrl) {
        return new ActionItem.Builder()
                .text(text)
                .subText((showAddress ? websiteUrl.toString() : null))
                .icon(iconRes)
                .setOnClickAction(createWebsiteOnClickAction(c, websiteUrl))
                .build();
    }

    public static MaterialAboutItemOnClickAction createRateOnClickAction(final Context c) {
        return () -> {
            // 跳转应用市场
            Intent intent = new Intent();
            intent.addFlags(Intent.FLAG_NOT_OHOS_COMPONENT);
            intent.setAction("android.intent.action.VIEW");
            intent.setUri(Uri.parse("market://search?q=App Name"));
            intent.addFlags(Intent.FLAG_NOT_OHOS_COMPONENT);
            c.startAbility(intent, 0);
        };
    }

    public static ActionItem createRateActionItem(Context c, int iconRes, String text, @Nullable String subText) {
        return new ActionItem.Builder()
                .text(text)
                .subText(subText)
                .icon(iconRes)
                .setOnClickAction(createRateOnClickAction(c))
                .build();
    }

    public static MaterialAboutItemOnClickAction createEmailOnClickAction(final Context c, String email, String emailSubject) {
//        String str = c.getString(ResourceTable.String_mal_send_email);
        String str = "Send email";
        return createEmailOnClickAction(c, email, emailSubject, str);
    }

    public static MaterialAboutItemOnClickAction createEmailOnClickAction(final Context c, String email, String emailSubject, final String chooserTitle) {
        return () -> new ToastDialog(c).setText("不支持发送邮件").show();
    }

    public static ActionItem createEmailItem(Context c, int iconRes, String text, boolean showEmail, String email, String emailSubject, String chooserTitle) {
        return new ActionItem.Builder()
                .text(text)
                .subText((showEmail ? email : null))
                .icon(iconRes)
                .setOnClickAction(createEmailOnClickAction(c, email, emailSubject, chooserTitle))
                .build();
    }

    public static ActionItem createEmailItem(Context c, int iconRes, String text, boolean showEmail, String email, String emailSubject) {
//        String str = c.getString(ResourceTable.String_mal_send_email);
        String str = "Send email";
        return createEmailItem(c, iconRes, text, showEmail, email, emailSubject, str);
    }

    public static MaterialAboutItemOnClickAction createPhoneOnClickAction(Context c, String number) {
        return () -> {
            Intent intents = new Intent();
            Operation operation = new Intent.OperationBuilder()
                    .withAction("ohos.intent.action.dial")
                    .withUri(Uri.parse("tel:" + number))
                    .build();
            intents.setOperation(operation);
            c.startAbility(intents, 0);
        };
    }

    public static ActionItem createPhoneItem(Context c, int iconRes, String text, boolean showNumber, String number) {
        return new ActionItem.Builder()
                .text(text)
                .subText((showNumber ? number : null))
                .icon(iconRes)
                .setOnClickAction(createPhoneOnClickAction(c, number))
                .build();
    }

    public static MaterialAboutItemOnClickAction createMapOnClickAction(final Context c, String addressQuery) {
        return () -> new ToastDialog(c).setText("不支持跳转地图").show();
    }

    public static ActionItem createMapItem(Context c, int iconRes, String text, String subText, String addressQuery) {
        return new ActionItem.Builder()
                .text(text)
                .subText(subText)
                .icon(iconRes)
                .setOnClickAction(createMapOnClickAction(c, addressQuery))
                .build();
    }

    public static MaterialAboutCard createLicenseCard(Context c, int iconRes, String libraryTitle, String year, String name, int licenseRes) {
        ActionItem licenseItem = new ActionItem.Builder()
                .icon(iconRes)
                .setIconAlignment(IconAlignment.ALIGNMENT_TOP.getValue())
                .text(libraryTitle)
                .subText(String.format(c.getString(licenseRes), year, name))
                .build();
        return new MaterialAboutCard.Builder().addItem(licenseItem).build();
    }

}
