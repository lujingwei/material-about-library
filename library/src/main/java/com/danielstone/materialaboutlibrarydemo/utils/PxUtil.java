package com.danielstone.materialaboutlibrarydemo.utils;

import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

import java.text.DecimalFormat;

public class PxUtil {
    private static Display display;

    public static void initContext(Context context) {
        display = DisplayManager.getInstance().getDefaultDisplay(context).get();
    }

    public static int screenWidth() {
        return display.getAttributes().width;
    }

    public static int screenHeight() {
        return display.getAttributes().height;
    }

    public static float fp2px(float fp) {
        float sca = display.getAttributes().scalDensity;
        return fp * sca + 0.5f * (fp >= 0 ? 1 : -1);
    }

    public static float vp2px(float vp) {
        float dpi = display.getAttributes().densityPixels;
        return vp * dpi + 0.5f * (vp >= 0 ? 1 : -1);
    }


    /**
     * 格式化数字(保留一位小数)
     *
     * @param num 数字
     * @return 格式化数字
     */
    public static String formatNum(int num) {
        DecimalFormat format = new DecimalFormat("0.0");
        return format.format(num);
    }

    /**
     * 格式化数字(保留两位小数)
     *
     * @param num 数字
     * @return 格式化数字
     */
    public static String formatNum(float num) {
        if (((int) num * 1000) == (int) (num * 1000)) {
            //如果是一个整数
            return String.valueOf((int) num);
        }
        DecimalFormat format = new DecimalFormat("0.00");
        return format.format(num);
    }
}
