package com.danielstone.materialaboutlibrarydemo.utils;

import com.danielstone.materialaboutlibrarydemo.ResourceTable;
import com.danielstone.materialaboutlibrarydemo.items.MaterialAboutItem;
import com.danielstone.materialaboutlibrarydemo.holders.ItemViewHolder;
import ohos.agp.components.Component;

public abstract class ViewTypeManager {

    public abstract int getLayout(int itemType);

    public abstract ItemViewHolder getViewHolder(int itemType, Component view);

    public abstract void setupItem(int itemType, ItemViewHolder holder, MaterialAboutItem item, Component parent);

    public static final class ItemType {
        public static final int ACTION_ITEM = 0;
        public static final int TITLE_ITEM = 1;
    }

    public static final class ItemLayout {
        public static final int ACTION_LAYOUT = ResourceTable.Layout_mal_material_about_action_item;
        public static final int TITLE_LAYOUT = ResourceTable.Layout_mal_material_about_title_item;
    }
}
