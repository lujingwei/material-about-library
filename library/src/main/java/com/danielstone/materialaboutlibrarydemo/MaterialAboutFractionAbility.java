package com.danielstone.materialaboutlibrarydemo;

import com.danielstone.materialaboutlibrarydemo.fraction.LicenseFraction;
import com.danielstone.materialaboutlibrarydemo.fraction.MaterialAboutFraction;
import com.danielstone.materialaboutlibrarydemo.utils.FractionHelper;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;

public class MaterialAboutFractionAbility extends FractionAbility {

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_fraction_container);
        FractionHelper.getInstance(this).addFraction(ResourceTable.Id_container, MaterialAboutFraction.class);
        FractionHelper.getInstance(this).addFraction(ResourceTable.Id_container, LicenseFraction.class);

        FractionHelper.getInstance(this).showFraction(MaterialAboutFraction.class);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
