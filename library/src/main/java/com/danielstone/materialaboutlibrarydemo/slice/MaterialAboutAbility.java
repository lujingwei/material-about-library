package com.danielstone.materialaboutlibrarydemo.slice;

import com.danielstone.materialaboutlibrarydemo.ConvenienceBuilder;
import com.danielstone.materialaboutlibrarydemo.ResourceTable;
import com.danielstone.materialaboutlibrarydemo.adapters.LicensesAdapter;
import com.danielstone.materialaboutlibrarydemo.adapters.MaterialAboutListAdapter;
import com.danielstone.materialaboutlibrarydemo.items.ActionItem;
import com.danielstone.materialaboutlibrarydemo.items.MyCustomItem;
import com.danielstone.materialaboutlibrarydemo.items.TitleItem;
import com.danielstone.materialaboutlibrarydemo.model.MaterialAboutCard;
import com.danielstone.materialaboutlibrarydemo.model.MaterialAboutList;
import com.danielstone.materialaboutlibrarydemo.snack.SnackBar;
import com.danielstone.materialaboutlibrarydemo.utils.MyViewTypeManager;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
//import ohos.agp.components.DirectionalLayoutManager;
import ohos.agp.components.ListContainer;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.ToastDialog;
import ohos.utils.net.Uri;

import java.util.ArrayList;
import java.util.List;

public class MaterialAboutAbility extends BaseSlice {

    private Component main;
    private ListContainer recyclerView;
    private MaterialAboutListAdapter adapter;
    private MaterialAboutList list;

    @Override
    protected void onStart(Intent intent) {
        intent.setParam("name", "About");
        super.onStart(intent);

        list = new MaterialAboutList();
        main = findComponentById(ResourceTable.Id_main);
        recyclerView = (ListContainer) findComponentById(ResourceTable.Id_mal_recyclerview);
        addBaseItem();
        addExtraItem();
        adapter = new MaterialAboutListAdapter(new MyViewTypeManager(), list.getCards());
//        recyclerView.setLayoutManager(new DirectionalLayoutManager());
        recyclerView.setItemProvider(adapter);
    }

    @Override
    public int getChildUIContent() {
        return ResourceTable.Layout_mal_material_about_content;
    }

    private void addBaseItem() {
        MaterialAboutCard app = new MaterialAboutCard.Builder()
                .addItem(new TitleItem.Builder()
                        .text("Material About Library")
                        .desc("© 2020 Daniel Stone")
                        .icon(ResourceTable.Media_icon)
                        .build()
                )
                .addItem(ConvenienceBuilder.createVersionActionItem(getAbility(),
                        ResourceTable.Media_icon, "Version", false))
                .addItem(new ActionItem.Builder()
                        .text("Changelog")
                        .icon(ResourceTable.Media_icon)
                        .setOnClickAction(ConvenienceBuilder.createWebViewDialogOnClickAction(
                                getAbility(),
                                "Releases",
                                "https://github.com/daniel-stoneuk/material-about-library/releases",
                                false,
                                false))
                        .build())
                .addItem(new ActionItem.Builder()
                        .text("Licenses")
                        .icon(ResourceTable.Media_icon)
                        .setOnClickAction(() -> {
                            present(new LicenseAbility(), new Intent().setParam("name", "Licenses"));
                        })
                        .build())
                .build();

        MaterialAboutCard author = new MaterialAboutCard.Builder()
                .addItem(new ActionItem.Builder()
                        .text("Daniel Stone")
                        .subText("United Kingdom")
                        .icon(ResourceTable.Media_icon)
                        .build())
                .addItem(new ActionItem.Builder()
                        .text("Fork on GitHub")
                        .icon(ResourceTable.Media_icon)
                        .setOnClickAction(ConvenienceBuilder.createWebsiteOnClickAction(
                                getAbility(), Uri.parse("https://github.com/daniel-stoneuk")))
                        .build())
                .title("Author")
                .build();

        MaterialAboutCard convenience = new MaterialAboutCard.Builder()
                .addItem(ConvenienceBuilder.createVersionActionItem(
                        getAbility(),
                        ResourceTable.Media_icon,
                        "Version",
                        false))
                .addItem(ConvenienceBuilder.createWebsiteActionItem(
                        getAbility(),
                        ResourceTable.Media_icon,
                        "Visit Website",
                        true,
                        Uri.parse("http://danstone.uk")))
                .addItem(ConvenienceBuilder.createRateActionItem(
                        getAbility(),
                        ResourceTable.Media_icon,
                        "Rate this app",
                        null
                ))
                .addItem(ConvenienceBuilder.createEmailItem(
                        getAbility(),
                        ResourceTable.Media_icon,
                        "Send an email",
                        true,
                        "apps@danstone.uk",
                        "Question concerning MaterialAboutLibrary"))
                .addItem(ConvenienceBuilder.createPhoneItem(
                        getAbility(),
                        ResourceTable.Media_icon,
                        "Call me",
                        true,
                        "+44 12 3456 7890"))
                .addItem(ConvenienceBuilder.createMapItem(
                        getAbility(),
                        ResourceTable.Media_icon,
                        "Visit London",
                        null,
                        "London Eye"))
                .title("Convenience Builder")
                .build();

        //暂不支持html 所以先用字符串代替
        String t1 = "This is HTML formatted text \n";
        String t2 = "This is very cool because it allows lines to get very long which can lead to all kinds of possibilities. \n";
        String t3 = "And line breaks. \n";
        String t4 = "Oh and by the way, this card has a custom defined background.";
        MaterialAboutCard other = new MaterialAboutCard.Builder()
                .addItem(new ActionItem.Builder()
                        .icon(ResourceTable.Media_icon)
                        .text("HTML Formatted Sub Text")
                        //暂不支持html 所以先用字符串代替
//                        .subTextHtml("This is <b>HTML</b> formatted <i>text</i> <br /> This is very cool because it allows lines to get very long which can lead to all kinds of possibilities. <br /> And line breaks. <br /> Oh and by the way, this card has a custom defined background.")
                        .subTextHtml(t1 + t2 + t3 + t4)
                        .setIconAlignment(ActionItem.ALIGNMENT_TOP)
                        .build())
                .cardColor(Color.getIntColor("#FFC0CFFF"))
                .outline(false)
                .title("Other")
                .build();

        list.addCard(app).addCard(author).addCard(convenience).addCard(other);
    }

    private void addExtraItem() {
        MaterialAboutCard.Builder advancedCardBuilder = new MaterialAboutCard.Builder();
        advancedCardBuilder.title("Advanced");

        advancedCardBuilder.addItem(new TitleItem.Builder()
                .text("TitleItem OnClickAction")
                .icon(ResourceTable.Media_icon)
                .setOnClickAction(ConvenienceBuilder.createWebsiteOnClickAction(getAbility(), Uri.parse("http://www.danstone.uk")))
                .build());

        advancedCardBuilder.addItem(new ActionItem.Builder()
                .text("Snackbar demo")
                .icon(ResourceTable.Media_icon)
                .setOnClickAction(() -> {
                    new SnackBar.Builder(getAbility(), main)
                            .withOnClickListener(null)
                            .withMessage("This is a new message! Snackbar demo")
                            .withBackgroundColorId(Color.BLACK.getValue())
                            .withDuration((short) 2000)
                            .withTypeFace(Font.DEFAULT)
                            .withAlignBottom(true)
                            .show();
                })
                .build());

        advancedCardBuilder.addItem(new ActionItem.Builder()
                .text("OnLongClickAction demo")
                .icon(ResourceTable.Media_icon)
                .build()
                .setOnLongClickAction(() -> new ToastDialog(getAbility()).setText("Long pressed").show()));

        advancedCardBuilder.addItem(new MyCustomItem.Builder()
                .text("Custom Item")
                .icon(ResourceTable.Media_icon)
                .build());

        advancedCardBuilder.addItem(createDynamicItem("Tap for a random number & swap position"));

        MaterialAboutCard.Builder customAdapterCardBuilder = new MaterialAboutCard.Builder();
//        // Create list of libraries
        List<String> libraries = new ArrayList<>();
        libraries.add("LicenseAdapter\r\n暂未提供下拉菜单组件");
        libraries.add("material-about-library\r\n暂未提供下拉菜单组件");

        customAdapterCardBuilder.title("Custom Adapter (License Adapter)");
        customAdapterCardBuilder.customAdapter(new LicensesAdapter(libraries));

        list.addCard(advancedCardBuilder.build());
        list.addCard(customAdapterCardBuilder.build());
    }

    private ActionItem createDynamicItem(String subText) {
        ActionItem item = new ActionItem.Builder()
                .text("Dynamic UI")
                .subText(subText)
                .icon(ResourceTable.Media_icon)
                .build();
        item.setOnClickAction(() -> {
            list.getCards().get(4).getItems().remove(list.getCards().get(4).getItems().indexOf(item));
            int newIndex = ((int) (Math.random() * 5));
            list.getCards().get(4).getItems().add(newIndex, item);
            item.setSubText("Random number: " + ((int) (Math.random() * 10)));
            adapter.notifyDataSetItemChanged(4);
        });

        return item;
    }

}
