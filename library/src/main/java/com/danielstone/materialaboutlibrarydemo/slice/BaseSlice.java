package com.danielstone.materialaboutlibrarydemo.slice;

import com.danielstone.materialaboutlibrarydemo.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;

public abstract class BaseSlice extends AbilitySlice {

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        DirectionalLayout layout = new DirectionalLayout(getContext());
        int par = DirectionalLayout.LayoutConfig.MATCH_PARENT;
        int con = DirectionalLayout.LayoutConfig.MATCH_CONTENT;
        layout.setLayoutConfig(new DirectionalLayout.LayoutConfig(par, par));
        Component header = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_base_slice, null, false);
        header.setLayoutConfig(new DirectionalLayout.LayoutConfig(par, con));
        Component child = LayoutScatter.getInstance(getContext()).parse(getChildUIContent(), null, false);
        layout.addComponent(header);
        layout.addComponent(child);
        setUIContent(layout);

        ((Text) findComponentById(ResourceTable.Id_nameTv)).setText(intent.getStringParam("name"));
        Button back = (Button) findComponentById(ResourceTable.Id_backBtn);
        back.setClickedListener(component -> {
            //关闭AbilitySlice
            this.terminate();
        });
    }

    public abstract int getChildUIContent();
}
