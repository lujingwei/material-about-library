package com.danielstone.materialaboutlibrarydemo.model;

import com.danielstone.materialaboutlibrarydemo.adapters.LicensesAdapter;
import com.danielstone.materialaboutlibrarydemo.items.MaterialAboutItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.UUID;

public class MaterialAboutCard {

    private String id = UUID.randomUUID().toString();

    private String title = null;
    private int titleRes = 0;

    private int titleColor = 0;
    private int cardColor = 0;
    private boolean outline = true;

    private LicensesAdapter customAdapter = null;
    private ArrayList<MaterialAboutItem> items = new ArrayList<>();

    private MaterialAboutCard(Builder builder) {
        this.title = builder.title;
        this.titleRes = builder.titleRes;
        this.titleColor = builder.titleColor;
        this.cardColor = builder.cardColor;
        this.items = builder.items;
        this.customAdapter = builder.customAdapter;
        this.outline = builder.outline;
    }

    public MaterialAboutCard(String title, MaterialAboutItem... materialAboutItems) {
        this.title = title;
        Collections.addAll(items, materialAboutItems);
    }

    public MaterialAboutCard(int titleRes, MaterialAboutItem... materialAboutItems) {
        this.titleRes = titleRes;
        Collections.addAll(items, materialAboutItems);
    }

    public MaterialAboutCard(MaterialAboutCard card) {
        this.id = card.getId();
        this.title = card.getTitle();
        this.titleRes = card.getTitleRes();
        this.titleColor = card.getTitleColor();
        this.cardColor = card.getCardColor();
        this.items = new ArrayList<>();
        this.outline = card.isOutline();
        this.customAdapter = card.getCustomAdapter();
        for (MaterialAboutItem item : card.items) {
            this.items.add(item.clone());
        }
    }

    public String getTitle() {
        return title;
    }

    public int getTitleRes() {
        return titleRes;
    }

    public int getTitleColor() {
        return titleColor;
    }

    public int getCardColor() {
        return cardColor;
    }

    public boolean isOutline() {
        return outline;
    }

    public ArrayList<MaterialAboutItem> getItems() {
        return items;
    }

    public String getId() {
        return id;
    }

    public LicensesAdapter getCustomAdapter() {
        return customAdapter;
    }

    @Override
    public String toString() {
        String result = "MaterialAboutCard{" +
                "id='" + id + '\'' +
                ", title=" + title +
                ", titleRes=" + titleRes +
                ", titleColor=" + titleColor +
                ", customAdapter=" + customAdapter +
                ", outline=" + outline +
                ", cardColor=" + cardColor + '}';
        return result;
    }

    public MaterialAboutCard clone() {
        return new MaterialAboutCard(this);
    }

    public static class Builder {
        private String title = null;

        private int titleRes = 0;

        private int titleColor = 0;

        private int cardColor = 0;

        private boolean outline = true;

        private ArrayList<MaterialAboutItem> items = new ArrayList<>();
        private LicensesAdapter customAdapter = null;

        public Builder title(String title) {
            this.title = title;
            this.titleRes = 0;
            return this;
        }

        public Builder title(int titleRes) {
            this.titleRes = titleRes;
            this.title = null;
            return this;
        }

        public Builder titleColor(int color) {
            this.titleColor = color;
            return this;
        }

        public Builder cardColor(int cardColor) {
            this.cardColor = cardColor;
            return this;
        }

        /**
         * Use outlined card design - true by default
         *
         * @param outline false to enable elevation
         * @return builder instance
         */
        public Builder outline(boolean outline) {
            this.outline = outline;
            return this;
        }

        public Builder addItem(MaterialAboutItem item) {
            this.items.add(item);
            return this;
        }

        public Builder customAdapter(LicensesAdapter customAdapter) {
            this.customAdapter = customAdapter;
            return this;
        }

        public MaterialAboutCard build() {
            return new MaterialAboutCard(this);
        }
    }

}
