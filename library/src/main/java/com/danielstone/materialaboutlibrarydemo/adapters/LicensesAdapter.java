package com.danielstone.materialaboutlibrarydemo.adapters;

import ohos.agp.components.*;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;

import java.util.List;

public class LicensesAdapter extends BaseItemProvider {

    private List<String> data;

    public LicensesAdapter(List<String> data) {
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer container) {
        Text text = new Text(container.getContext());
        int matchParent = ComponentContainer.LayoutConfig.MATCH_PARENT;
        int matchContent = ComponentContainer.LayoutConfig.MATCH_CONTENT;
        DirectionalLayout.LayoutConfig config = new DirectionalLayout.LayoutConfig(matchParent, matchContent);
        config.alignment = LayoutAlignment.CENTER;
        config.setMargins(50, 50, 50, 50);
        text.setLayoutConfig(config);
        text.setTextSize(50);
        text.setTextColor(Color.BLACK);
        text.setMultipleLine(true);
        text.setText(data.get(i));
        return text;
    }
}
