package com.danielstone.materialaboutlibrarydemo.adapters;

import com.danielstone.materialaboutlibrarydemo.ResourceTable;
import com.danielstone.materialaboutlibrarydemo.model.MaterialAboutCard;
import com.danielstone.materialaboutlibrarydemo.utils.DefaultViewTypeManager;
import com.danielstone.materialaboutlibrarydemo.utils.ViewTypeManager;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;

import java.util.List;

public class MaterialAboutListAdapter extends BaseItemProvider {

    private List<MaterialAboutCard> data;
    private ViewTypeManager viewTypeManager;
    private LayoutScatter instance;

    public MaterialAboutListAdapter(List<MaterialAboutCard> newData) {
        this.data = newData;
        this.viewTypeManager = new DefaultViewTypeManager();
    }

    public MaterialAboutListAdapter(ViewTypeManager customViewTypeManager, List<MaterialAboutCard> newData) {
        this.data = newData;
        this.viewTypeManager = customViewTypeManager;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer container) {
        if (instance == null) {
            instance = LayoutScatter.getInstance(container.getContext());
        }
        Component itemView = instance.parse(ResourceTable.Layout_mal_material_about_list_card, null, false);
        ListViewHolder viewHolder = new ListViewHolder(itemView);
        onBindViewHolder(viewHolder, i);
        return itemView;
    }

    private void onBindViewHolder(ListViewHolder holder, int position) {
        MaterialAboutCard card = data.get(position);

        String title = card.getTitle();
        int titleRes = card.getTitleRes();
        holder.title.setVisibility(Component.VISIBLE);
        if (title != null) {
            holder.title.setText(title);
        } else if (titleRes != 0) {
            holder.title.setText(titleRes);
        } else {
            holder.title.setVisibility(Component.HIDE);
        }

        int titleColor = card.getTitleColor();
        if (holder.title.getVisibility() == Component.VISIBLE) {
            if (titleColor != 0) {
                holder.title.setTextColor(new Color(titleColor));
            } else {
                holder.title.setTextColor(Color.BLACK);
            }
        }

        int cardColor = card.getCardColor();
        Context context = holder.title.getContext();
        ShapeElement element = new ShapeElement(context, ResourceTable.Graphic_shape_bg_main_card);
        element.setRgbColor(RgbColor.fromArgbInt(cardColor));
        if (cardColor != 0) {
            element.setRgbColor(RgbColor.fromArgbInt(cardColor));
            holder.container.setBackground(element);
        }

        if (card.getCustomAdapter() != null) {
            LicensesAdapter newAdapter = card.getCustomAdapter();
            if (holder.adapter == null || !(holder.adapter.getClass().isInstance(newAdapter))) {
                holder.list.setItemProvider(newAdapter);
            }
        } else {
            if (!(holder.adapter instanceof MaterialAboutItemAdapter)) {
                holder.adapter = new MaterialAboutItemAdapter(viewTypeManager, card.getItems());
                holder.list.setItemProvider(holder.adapter);
            }
        }
    }

    class ListViewHolder {
        Text title;
        ListContainer list;
        DirectionalLayout container;
        BaseItemProvider adapter;

        ListViewHolder(Component itemView) {
            container = (DirectionalLayout) itemView.findComponentById(ResourceTable.Id_container);
            title = (Text) itemView.findComponentById(ResourceTable.Id_mal_list_card_title);
            list = (ListContainer) itemView.findComponentById(ResourceTable.Id_mal_card_recyclerview);
//            list.setLayoutManager(new DirectionalLayoutManager());
        }
    }
}
