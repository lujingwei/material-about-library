package com.danielstone.materialaboutlibrarydemo.fraction;

import com.danielstone.materialaboutlibrarydemo.ResourceTable;
import com.danielstone.materialaboutlibrarydemo.utils.FractionHelper;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;

public abstract class BaseFraction extends Fraction {

    private DirectionalLayout layout;

    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        layout = new DirectionalLayout(getContext());
        int par = DirectionalLayout.LayoutConfig.MATCH_PARENT;
        int con = DirectionalLayout.LayoutConfig.MATCH_CONTENT;
        layout.setLayoutConfig(new DirectionalLayout.LayoutConfig(par, par));
        Component header = LayoutScatter.getInstance(getFractionAbility()).parse(ResourceTable.Layout_base_slice, null, false);
        header.setLayoutConfig(new DirectionalLayout.LayoutConfig(par, con));
        Component child = LayoutScatter.getInstance(getFractionAbility()).parse(getChildUIContent(), null, false);
        layout.addComponent(header);
        layout.addComponent(child);
        return layout;
    }

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        ((Text) layout.findComponentById(ResourceTable.Id_nameTv)).setText(intent.getStringParam("name"));
        Button back = (Button) layout.findComponentById(ResourceTable.Id_backBtn);
        back.setClickedListener(component -> {
            //关闭AbilitySlice
            Fraction fraction = FractionHelper.getInstance(getFractionAbility()).gotoLastFraction();
            if(fraction == null){
                getFractionAbility().terminateAbility();
            }
        });
    }

    public abstract int getChildUIContent();
}
