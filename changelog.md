## 0.0.1-SNAPSHOT

LikeStarAnimation组件openharmony beta版本

#### api差异

* 无

#### 已实现功能

* 详见demo

#### 未实现功能

* 未实现切换主题
* 未实现可折叠展开的选项
* 未实现打开WebView页面，未实现加载Html页面或者Html片段
* 未实现跳转到地图和邮件

