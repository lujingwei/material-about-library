package com.danielstone.materialaboutlibrarydemo;

import com.danielstone.materialaboutlibrarydemo.utils.PxUtil;
import junit.framework.TestCase;
import org.junit.Assert;

public class ExampleTest extends TestCase {

    public void testFormatNum1() {
        int num = 10;
        String result = PxUtil.formatNum(num);
        Assert.assertNotNull(result);
        Assert.assertEquals("10.0", result);
    }

    public void testFormatNum2() {
        float num = 10.543f;
        String result = PxUtil.formatNum(num);
        Assert.assertNotNull(result);
        Assert.assertEquals("10.54", result);
    }

}
