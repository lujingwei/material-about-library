package com.danielstone.materialaboutlibrarydemo;

import com.danielstone.materialaboutlibrarydemo.utils.PxUtil;
import ohos.aafwk.ability.AbilityPackage;

public class MyApplication extends AbilityPackage {
    @Override
    public void onInitialize() {
        super.onInitialize();
        PxUtil.initContext(this);
    }
}
