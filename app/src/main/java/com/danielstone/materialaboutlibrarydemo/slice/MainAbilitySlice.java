package com.danielstone.materialaboutlibrarydemo.slice;

import com.danielstone.materialaboutlibrarydemo.MaterialAboutFractionAbility;
import com.danielstone.materialaboutlibrarydemo.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Button;

public class MainAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        Button btn1 = (Button) findComponentById(ResourceTable.Id_fragment_button);
        btn1.setClickedListener(component -> {
            Intent intents = new Intent();
            Operation operation = new Intent.OperationBuilder()
                    .withBundleName(getBundleName())
                    .withAbilityName(MaterialAboutFractionAbility.class.getName())
                    .build();
            intents.setOperation(operation);
            startAbility(intents, 0);
        });

        Button btn2 = (Button) findComponentById(ResourceTable.Id_button1);
        btn2.setClickedListener(component -> present(new MaterialAboutAbility(), new Intent()));

    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
