# material-about-library

#### 项目介绍
- 项目名称：material-about-library
- 所属系列：openharmony的第三方组件适配移植
- 功能：material-about-library库包含了多种样式的选项条，多用于“关于”页面
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk5，DevEco Studio2.1 beta3
- 基线版本：Release 3.2.0-rc01

#### 效果演示

![效果演示](https://images.gitee.com/uploads/images/2021/0526/103948_acde3e6e_1659014.gif "截图.gif")
![效果演示](https://images.gitee.com/uploads/images/2021/0526/104015_c494a8e9_1659014.gif "截图.gif")

#### 安装教程

方式一：

1.在项目根目录下的build.gradle文件中，

```
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/snapshots/'
        }
    }
}
```

2.在entry模块的build.gradle文件中，

```
 dependencies {
    implementation('com.gitee.chinasoft_ohos:material-about-library:0.0.1-SNAPSHOT')
    ......  
 }
```

在sdk5，DevEco Studio2.1 beta3下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明


```java
作为Fraction调用:

Intent intents = new Intent();
Operation operation = new Intent.OperationBuilder()
           .withBundleName(getBundleName())
           .withAbilityName(MaterialAboutFractionAbility.class.getName())
           .build();
intents.setOperation(operation);
startAbility(intents, 0);

MaterialAboutFractionAbility集成FractionAbility,并添加如下代码:

@Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_fraction_container);
        FractionHelper.getInstance(this).addFraction(ResourceTable.Id_container, MaterialAboutFraction.class);
        FractionHelper.getInstance(this).showFraction(MaterialAboutFraction.class);
    }
```

```java
作为AbilitySlice调用:

present(new MaterialAboutAbility(), new Intent());
```

完整调用详见 [MaterialAboutAbility](https://gitee.com/chinasoft_ohos/material-about-library/blob/master/library/src/main/java/com/danielstone/materialaboutlibrarydemo/slice/MaterialAboutAbility.java), [MaterialAboutFraction](https://gitee.com/chinasoft_ohos/material-about-library/blob/master/library/src/main/java/com/danielstone/materialaboutlibrarydemo/fraction/MaterialAboutFraction.java)


#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

火绒安全病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代

- 0.0.1-SNAPSHOT


#### 版权和许可信息

```
Copyright 2016-2020 Daniel Stone

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
